PIPFILE_EXTRA_DRF = ''
if {{ cookiecutter.use_drf }}:
  PIPFILE_EXTRA_DRF = '''drf-yasg = "*"
djangorestframework = "*"
django-rest-auth = "*"'''

PIPFILE_EXTRA_ALLAUTH = ''
if {{ cookiecutter.use_allauth }}:
  PIPFILE_EXTRA_ALLAUTH = '''django-allauth = "*"'''

PIPFILE = '''
[[source]]
name = "pypi"
url = "https://pypi.org/simple/"
verify_ssl = true

[dev-packages]
pylint = "<5"
pylint-django = "*"
mypy = "*"
django-stubs = "*"
mock = "*"
bump2version = "==0.5.11"
pyyaml = "*"
pyterprise = "*"
hvac = "*"
coverage = "*"
django-coverage-plugin = "*"
pytest = "*"
pytest-django = "*"
pytest-pythonpath = "*"
pytest-coverage = "*"


[packages]
google-auth = "*"
google-cloud-iam = "*"
whitenoise = "*"
validator-collection = "*"
requests = {extras = ["security"],version = "*"}
psycopg2-binary = "*"
django = "~=3.0"
django-debug-toolbar = "*"
django-vault-helpers = "*"
boto3 = "*" # Broken dependency for django-vault-helpers, not even used
django-cors-headers = "*"
django-environ = "*"
django-filter = "*"
dj-database-url = "*"
gunicorn = "==20.0.4"
django-management-tools = "*"
vault2env = "==0.9.2"
stackdriver-formatter = "*"
%s
%s

[requires]
python_version = "3.8"

[scripts]
collectstatic = "python src/manage.py collectstatic --no-input"
'''%(PIPFILE_EXTRA_ALLAUTH, PIPFILE_EXTRA_DRF)

with open('Pipfile', 'w+') as filed:
  filed.write(PIPFILE)
