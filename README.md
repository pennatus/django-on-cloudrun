# Usage

This project brings up a Django instance that uses Docker on GCP CloudRun and integrates with the [Pennatus gcp-scaffold](https://gitlab.com/pennatus/gcp-cookiecutter) that integrates HashiCorp Vault with GCP and Gitlab to create a Cloud Native app under CI/CD with secret management.

To use this scaffold requires preparing your infrastructure with

* New product using [PFM](https://gitlab.com/pennatus/terraform-modules/terraform-product-factory)
* A database ([example](https://gitlab.com/pennatus/terraform-modules/terraform-postgres-db))
* A [CI/CD AppRole](https://gitlab.com/pennatus/terraform-modules/terraform-vault-auth/-/tree/master/) to issue credentials for CI/CD and local access to CloudSQL


## Update Terraform infrastructure

In the `is-org` workspace you can use the Product Factory Module to add a new product definition that may look something like the following:

``` hcl
module "mywebsite_product" {
    source = "git::https://gitlab.com/pennatus/terraform-modules/terraform-product-factory"

    MONITORING_PROJECT_ID = module.org_project.org_project_id
    PRODUCT_NAME = "mywebsite"
    BILLING_DISPLAY_NAME = var.BILLING_NAME_GENERAL
    DOMAIN = var.DOMAIN
    FOLDER_IDS = module.org_project.folder_ids
    ENVIRONMENTS = [ "prd", "stg" ]
    APIS = [ "containerregistry", "run" ]
    SSH_KEYS = [ ]
    KV_SECRET_PATHS = [ "django" ]
}
```

Also, make sure you have a database for each of your environments above:

```hcl
module "shared_db" {
    source = "git::https://gitlab.com/pennatus/terraform-modules/terraform-postgres-db"

    PROJECT_ID = module.shared_product.env_project_map.prd.id
    NAME = "shared-db"
    DATABASES = [ "mydb_stg", "mydb_prd" ]
    GCP_TOKEN = local.GCP_TOKEN
    VAULT_PROJECT_ID = data.terraform_remote_state.is_vault.outputs.vault_project_id
    VAULT_ADDRESS = data.terraform_remote_state.is_vault.outputs.vault_url
    VAULT_TOKEN = var.VAULT_ROOT_TOKEN
}
```

Snippet of AppRole mapping in the `APPROLE_VAULTPOLICY_MAP` input variable which will provide write access to the Google Container Registry:

``` hcl
# Gitlab Approle policies for each product.

"gitlab-mywebsite": [
      module.product_mywebsite.vault_editor_policies.prd.name,
      module.product_mywebsite.vault_editor_policies.stg.name,
      module.product_mywebsite.vault_editor_policies.dev.name
    ],

# Credentials for terraform.io to authenticate with AppRole.  Project owner
# allows terraform to manage the full cycle of resources.

"terraform-mywebsite-prd": [ module.product_mywebsite.vault_owner_policies.prd.name,
                            module.org_project.vault_token_creator_policies.name ]
"terraform-mywebsite-stg": [ module.product_mywebsite.vault_owner_policies.stg.name,
                            module.org_project.vault_token_creator_policies.name ]
"terraform-mywebsite-dev": [ module.product_mywebsite.vault_owner_policies.dev.name,
                            module.org_project.vault_token_creator_policies.name ]

# Using the Vault GCP Auth Method, we can map a set of policies (paths) to a particular project giving access to resources like CloudRun, Computer, etc so that credentials such as KV2 stores, dynamic database credentials, etc. can be accessed.
GCPPROJECT_VAULTPOLICY_MAP = {
  mywebsite-prd-gcp-policies = {
    project_id = module.product_mywebsite.env_project_map.prd.id
    token_policies = [
    module.product_mywebsite.vault_viewer_policies.prd.name,
    module.shared_db.vault_viewer_policies.mydb_prd.name
    ]
  mywebsite-stg-gcp-policies = {
    project_id = module.product_mywebsite.env_project_map.stg.id
    token_policies = [
    module.product_mywebsite.vault_viewer_policies.stg.name,
    module.shared_db.vault_viewer_policies.mydb_stg.name
    ]
  mywebsite-dev-gcp-policies = {
    project_id = module.product_mywebsite.env_project_map.dev.id
    token_policies = [
    module.product_mywebsite.vault_viewer_policies.dev.name,
    module.shared_db.vault_viewer_policies.mydb_dev.name
    ]
}
```

Now create the terraform.io workspaces for each environment to allow independent management of each environment's infrastructure.  There is script that comes with the `prd/is-org` cookiecutter directory that automatically creates the workspaces.

``` sh
cd MyOrg/infrastructure # A clone of your IS repository
pipenv install
# MyOrg is a case sensitive match of your Terraform.io Org
# prd-is-org must match your terraform.io infrastructure workspace
# mywebsite must match your product name used in the previous terraform changes
# Use --help for more information
pipenv run tf-workspaces.py create MyOrg prd-is-org mywebsite dev stg prd
```

The last step is to make sure the is-org workspace exports the project ID so it can be used in the boilerplate terraform code included with this template (these values are used for `terraform_output_var_name_prd_project_id` and `terraform_output_var_name_org_project_id`):

``` hcl
# outputs.tf
output "org_project_id" {
  value = module.org_project.org_project_id
  description = "This is the full project id of the main org project"
}

output "prd_mywebsite_project_id" {
  value = module.product_mywebsite.env_project_map.prd.id
  description = "Production GCP Project ID for the `mywebsite` project."
}

output "stg_mywebsite_project_id" {
  value = module.product_mywebsite.env_project_map.stg.id
  description = "Staging GCP Project ID for the `mywebsite` project."
}

output "dev_mywebsite_project_id" {
  value = module.product_mywebsite.env_project_map.dev.id
  description = "Development GCP Project ID for the `mywebsite` project review apps."
}
```

Once all the Terraform changes have planned and applied successfully you are ready to move on to the Python code (and a little more Terraform).

## Create the Django project

Install Cookiecutter, for example:

``` sh
pip install "cookiecutter>=1.4.0"
```

Now run it against this repo:

``` sh
cookiecutter https://gitlab.com/pennatus/django-on-cloudrun
```

You'll be prompted for some values. Provide them, then a Django project will be created for you.

**Warning**: The default values presented by cookiecutter are not good defaults, read carefully and refer to the below description for more details if uncertain.

`product_name_slug` is the 'nicely formatted' product_name that will be used in vault paths and other path names.  This means things like removing spaces and making lowercase are important.

`gitlab_project_slug` is the project name to store the files under in Gitlab.

`use_drf` if true, adds additional Python modules and code to enable Django Rest Framework.  Choose yes if you would like to use DRF.

`use_allauth` if true, adds additional Python modules and code to enable the popular Django All Auth package.  Choose yes  if you would like to use All Auth.

`db_instance_name` is the name of the GCP postgres instance that will contain the database for Django.  The instance can be in any project at GCP provided the service account for CloudRun is granted access to the Cloud SQL Proxy role for that project.

`product_prd|stg|dev_project_id` is the specific GCP project id that is hosting the GCP infrastructure for the product.

`db_instance_project_id` is the specific GCP project id hosting the database instance named in `db_instance_name`.

`vault_addr` is the full URL to reach the Vault server.

`gcp_roleset` is the name of the GCP roleset configured in the Vault server which provides GCP credentials bound to the project owner role. Use

`terraform_org_name` is the organization name in Terraform.io.

`terraform_is_org_workspace_name` is the name of the workspace in Terraform.io used for the org infrastructure.

`terraform_is_org_workspace_name` is the name of the workspace in Terraform.io used for the vault infrastructure.

`terraform_output_var_name_prd_project_id` is the variable name in outputs.tf file of the is-org workspace that contains the value for GCP project id for the production product.

`terraform_output_var_name_org_project_id` is the variable name in outputs.tf file of the is-org workspace that contains the value for GCP project id for the is-org product.

Answer the prompts with your own desired options_. For example::

``` console
product_name_slug [name_of_product]:
gitlab_project_slug [myproject]:
use_drf [0, 1]:
use_allauth [0, 1]:
db_instance_name [shared-db]:
product_prd_project_id [prd-myproduct-12345]:
product_stg_project_id [stg-myproduct-12345]:
product_dev_project_id [dev-myproduct-12345]:
db_instance_project_id [prd-company-12345]:
vault_addr [https://vault.myorg.com]:
gcp_roleset [project-ow]:
terraform_org_name [MyCompany]:
terraform_is_org_workspace_name [prd-is-org]:
terraform_is_vault_workspace_name [prd-is-vault]:
terraform_output_var_name_prd_project_id [name_of_product-project-id]:
terraform_output_var_name_org_project_id [org-12345]:
```

Enter the project and take a look around::

``` sh
cd myproject/
ls
```

Create a git repo and push it there::

``` sh
git init
git add .
git commit -m "first awesome commit"
git remote add origin git@gitlab.com:pennatus/myproject.git
git push -u origin master
```

Now take a look at your repo.

For development, see the following:

``` sh
make help
```

Next you will need to configure the `.env` file with your admin approle roleid and secretid.  This will allow you to `make build/push/deploy/test` and connect to the live database.

Finally, you can use `make bumpversion-[patch|minor|major]` to update, commit, and tag.  Once you push, that should trigger Gitlab to push a tagged release to production.  Currently, this is not setup for staging.
