# cms Infrastructure

This directory holds the Terraform code which generates the CloudRun service within the project for this environment.  This can be duplicated to other environments by searching for `prd` and replacing with the target environment name, double check the value for the project ID because the prd substring may not be present (ex: `data.terraform_remote_state.is_organisation.projectname_project_id`)

Before commiting, run `make gen` to produce the I/O section of this document.

This module assumes you are defining the following outputs in the `is_org` workspace:

* shared_db_instance_name - the DB instance name that hosts the database
* shared_db_connection_name - the CloudSQL connection string to the database
* The project ID for your production and staging (specified during cookiecutter phase)

The following outputs are requred from the `is_vault` workspace:

* vault_url - the URL to access the Vault instance

## I/O

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| DEPLOYED\_VERSION | Set this to a valid Docker tag and queue plan to deploy a new version. | string | `""` | no |
| VAULT\_APPROLE\_ROLE\_ID | A role ID is attached to a set of Vault policies to read secrets | string | `""` | no |
| VAULT\_APPROLE\_SECRET\_ID | A secret is issued by vault for a specific role ID | string | `""` | no |

## Outputs

| Name | Description |
|------|-------------|
| status | A [status object](https://cloud.google.com/run/docs/reference/rest/v1/Status) which contains the URL and other data. |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
