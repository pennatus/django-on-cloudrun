# Setup our remote state link so we can pull stuff from the {{ cookiecutter.terraform_is_org_workspace_name }} workspace
data "terraform_remote_state" "is_organization" {
  backend = "remote"

  config = {
    organization = "{{ cookiecutter.terraform_org_name }}"
    workspaces = {
      name = "{{ cookiecutter.terraform_is_org_workspace_name }}"
    }
  }
}
data "terraform_remote_state" "is_vault" {
  backend = "remote"

  config = {
    organization = "{{ cookiecutter.terraform_org_name }}"
    workspaces = {
      name = "{{ cookiecutter.terraform_is_vault_workspace_name }}"
    }
  }
}

provider "vault" {
  address = data.terraform_remote_state.is_vault.outputs.vault_url
  auth_login {
    path = "auth/approle/login"
    parameters = {
      role_id   = var.VAULT_APPROLE_ROLE_ID
      secret_id = var.VAULT_APPROLE_SECRET_ID
    }
  }
}

# If the project has been setup for us and our vault token has the right permission then
# the following code will read a new GCP access token that has Owner privilege to this project.
data "vault_generic_secret" "gcp_token" {
  path = "org/gcp/token/prd-{{ cookiecutter.product_name_slug[0:7] }}-ow"
}

# Provided the gcp_token can be read then our google provider will be ready to go and we can continue.
provider "google" {
  access_token = data.vault_generic_secret.gcp_token.data["token"]
  project      = data.terraform_remote_state.is_organization.outputs.{{ cookiecutter.terraform_output_var_name_prd_project_id }}
  version      = "~> 3.24.0"
}

provider "google-beta" {
  access_token = data.vault_generic_secret.gcp_token.data["token"]
  project      = data.terraform_remote_state.is_organization.outputs.{{ cookiecutter.terraform_output_var_name_prd_project_id }}
  version      = "~> 3.24.0"
}

data "google_project" "{{cookiecutter.product_name_slug}}_project" {
  project_id = data.terraform_remote_state.is_organization.outputs.{{ cookiecutter.terraform_output_var_name_prd_project_id }}
}

locals {
  project_id = data.terraform_remote_state.is_organization.outputs.{{ cookiecutter.terraform_output_var_name_prd_project_id }}
  # Name of GCP DB instance
  db_instance_name = data.terraform_remote_state.is_organization.outputs.shared_db_instance_name
  db_connection_name = data.terraform_remote_state.is_organization.outputs.shared_db_connection_name
  db_name = "{{ cookiecutter.prd_db_name }}"
  product_name = "{{ cookiecutter.product_name_slug }}"
  product_name_slug = "{{ cookiecutter.product_name_slug }}"
  # Tune these to get maximum CPU and memory utilization
  workers = 2
  threads = 4
  cpu    = "1000m"
  memory = "512Mi"
}


resource "google_cloud_run_service" "{{ cookiecutter.product_name_slug }}" {
  name     = local.product_name
  provider = google-beta
  location = "us-central1"

  metadata {
    namespace = local.project_id
  }

  template {
      # metadata {
          # Name the revision so it can be traffic controlled
          # Note this is subject to validation: 253 chars, alphanumeric, '-' and '.' only
          # name = "${var.DEPLOYED_VERSION}-b${local.build_timestamp}"
      # }

    spec {

      container_concurrency = local.workers * local.threads

      containers {
        image = "gcr.io/${local.project_id}/${local.product_name_slug}:${var.DEPLOYED_VERSION}"
        resources {
          limits = {
            cpu    = local.cpu
            memory = local.memory
          }
        }
        env {
          # One of dev, stg or prd
          name = "DEPLOY_ENV"
          value = "prd"
        }
        env {
          name = "CLOUDSQL_DB_INSTANCE_NAME"
          value = local.db_connection_name
        }
        env {
          name = "DATABASE_URL"
          value = "postgres://127.0.0.1:5432/${local.db_name}"
        }
        env {
          name = "PROJECT_ID"
          value = local.project_id
        }
        env {
          name = "VAULT_ADDR"
          value = data.terraform_remote_state.is_vault.outputs.vault_url
        }
        env {
          # Path to get credentials for database
          name = "VAULT_DATABASE_PATH"
          value = "org/db/${local.db_instance_name}/creds/${local.db_name}-owner"
        }
        env {
          name = "GUNICORN_WORKERS"
          value = local.workers
        }
        env {
          name = "GUNICORN_THREADS"
          value = local.threads
        }
        env {
          name = "VERSION"
          value = var.DEPLOYED_VERSION
        }
      }
    }
  }
}

data "google_iam_policy" "noauth" {
  binding {
    role = "roles/run.invoker"
    members = [
      "allUsers",
    ]
  }
}

resource "google_cloud_run_service_iam_policy" "noauth" {
  location    = google_cloud_run_service.{{ cookiecutter.product_name_slug }}.location
  project     = google_cloud_run_service.{{ cookiecutter.product_name_slug }}.project
  service     = google_cloud_run_service.{{ cookiecutter.product_name_slug }}.name

  policy_data = data.google_iam_policy.noauth.policy_data
}

