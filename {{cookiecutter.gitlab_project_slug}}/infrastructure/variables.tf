variable "DEPLOYED_VERSION" {
    type = string
    description = "Set this to a valid Docker tag and queue plan to deploy a new version."
    default = ""
}

variable "VAULT_APPROLE_SECRET_ID" {
    type = string
    description = "A secret is issued by vault for a specific role ID"
    default = ""
}

variable "VAULT_APPROLE_ROLE_ID" {
    type = string
    description = "A role ID is attached to a set of Vault policies to read secrets"
    default = ""
}