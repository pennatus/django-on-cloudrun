output "status" {
  value = google_cloud_run_service.{{ cookiecutter.product_name_slug }}.status
  description = "This status which contains URL of the Cloud Run instance."
}
