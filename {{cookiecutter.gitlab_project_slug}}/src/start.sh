#!/bin/bash
CONTAINER_ID=${RANDOM}
VERSION=${VERSION:-not_set}
echo "Starting container ${RANDOM} for version ${VERSION}"

die() {
  echo "!!! ${*}"
  exit 1
}

if [ "$DEPLOY_ENV" == "dev" ]; then
  WORKERS=1
  THREADS=4
else
  WORKERS=${GUNICORN_WORKERS:-2}
  THREADS=${GUNICORN_THREADS:-4}
fi

export VAULT_URL=${VAULT_ADDR:-$VAULT_URL}

cd src

if [ "${GCP_TOKEN}" != "" ]; then
  # If the GCP_TOKEN is passed in, use that, otherwise assume that cloudsql_proxy
  # will use the credentials from the environment.
  LOCAL_GCP_TOKEN="-token=${GCP_TOKEN}"
fi
# You can simulate ADC by passing in GCP_TOKEN, ACCOUNT and PROJECT_ID and
# the tool will get a vault token, otherwise the tool just gets the default
# credentials from the enviornment and magic ensues.
result="$(python ./get_vault_token.py)"
GCP_TOKEN=${GCP_TOKEN:-$(echo "$result" | jq -r '.gcp.token')}
VAULT_TOKEN=${VAULT_TOKEN:-$(echo "$result" | jq -r '.vault.token')}

if [ "$VAULT_TOKEN" == "" ]; then
  echo "Unable to get a VAULT_TOKEN, either set VAULT_TOKEN, run in an ADC environemnt or set (GCP_TOKEN, ACCOUNT and PROJECT_ID)"
  echo "The vault2env service and cloudsql_proxy will be unavailable"
fi

# Connect to Cloud SQL if possible, DATABASE_URL should be set in all cases.
if [ "$CLOUDSQL_DB_INSTANCE_NAME" != "" -a "$GCP_TOKEN" != "" ]; then
  cloud_sql_proxy $LOCAL_GCP_TOKEN -instances=$CLOUDSQL_DB_INSTANCE_NAME=tcp:5432&
  sleep .2; jobs %1 || die "cloud_sql_proxy crashed ($?)!"
elif [ "$DATABASE_URL" == "" -a "$GCP_TOKEN" != "" ]; then
  die "Must specify either a database URL or a Cloud SQL Instance connection with a valid GCP_TOKEN"
fi

# Attempt get a .env from Vault if it exists
vault2env -a ${VAULT_URL} -t $VAULT_TOKEN -m product/${DEPLOY_ENV}/{{ cookiecutter.product_name_slug }}/kv-v2/{{ cookiecutter.vault_env_kv2_name }} -p env -o .env

# Used by vaulthelpers: the name of the role to assume after connecting using SET ROLE
export VAULT_TOKEN DATABASE_URL VAULT_URL DEPLOY_ENV CLOUDSQL_DB_INSTANCE_NAME CONTAINER_ID VERSION

# Run any extra stuff that we want before starting
# the main django server
run-parts start.d

exec gunicorn --bind=0.0.0.0:8080 \
              --workers=$WORKERS \
              --threads=$THREADS wsgi
