# Import the default settings here - don't touch this file
from settings_default import *

ENV_SCHEMA = {
  # Add any project specific environment variables here
  'CUSTOM_ENV' : (str, 'Custom environment variable'),
}
env = environ.Env(**ENV_SCHEMA)

INSTALLED_APPS += [
  # Add our custom apps here
  'apps.hello'
]

# Grab the custom variables out of the schema
CUSTOM_ENV = env('CUSTOM_ENV')