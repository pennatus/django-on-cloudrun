import logging
import time
import json
import os
import sys
from typing import Optional

import requests

from google import auth
from google.cloud import iam_credentials_v1

logging.basicConfig()
log = logging.getLogger(os.path.basename(__file__).replace('.py',''))

def usage() -> None:
    doc='''get_vault_token.py - Use GCP credentials to get a Vault token

    Cloud Run, Compute Engine, App Engine, etc. have credentials baked into the system.
    Those credentials can be accessed using the google-auth library and they provide all
    the details that are present in a JSON service account file.  You can simulate this
    by setting GOOGLE_APPLICATION_CREDENTIALS to point to a service account key file and
    then get a Vault token.

    The app expects your Vault address to defined at VAULT_ADDR or VAULT_URL, with VAULT_URL
    taking predence over VAULT_ADDR.

    You can override the default credentials by specifying the coordinates in your environment.
    To get a signed JWT token which is the login method used to get a Vault token, you need the
    following environment variables:

    ``` sh
    $ ACCOUNT=543332111111-compute@developer.gserviceaccount.com # service account email address
    $ PROJECT_ID=stg-koolapp-12345 # Project ID that the ACCOUNT can sign for
    $ GCP_TOKEN=ya29.c.Ko8BvQde03buelotsmorelettersandnumbersthatkeepgoingandgoing
    $ get_vault_token.py | jq
    {
    "gcp": {
        "token": "ya29.c.Ko8BvQde03buelotsmorelettersandnumbersthatkeepgoingandgoing",
        "account": "543332111111-compute@developer.gserviceaccount.com",
        "project_id": "stg-koolapp-12345",
        "jwt": "eyJhbGciOiJSUthisisaJOSEtokenwiththreepartsthathasbeensignedbyGCP"
    },
    "vault": {
        "token": "s.zaServiceTokenFromVault"
    }
    }
    ```

    The output is JSON formatted to stdout and errors go to stderr for debugging.
    '''
    log.info(doc)

def get_vault_token(jwt:str, project_id:str, vault_address:str) -> str:
    '''
    Use a signed JWT with for a service account to login to vault and get a Vault token
    '''
    body={ "role":project_id,
           "jwt":jwt,
         }
    headers={"Content-Type": "application/json"}
    response = requests.post(f"{vault_address}/v1/auth/gcp/login", json=body, headers=headers)

    if response.status_code != 200:
        log.error(f"Vault ERROR response: {response.json()}")
        response.raise_for_status()

    auth_body = response.json().get('auth')
    if not auth_body:
        raise ValueError('Missing or empty "auth" key in JSON response from Vault')

    return auth_body.get('client_token')

def get_signed_jwt(claim:str, project_id:str, account:Optional[str], gcp_token:Optional[str]) -> str:
    '''
    Use a signed JWT with for a service account to login to vault and get a Vault token
    '''

    payload = { "payload" : claim }
    headers={ "Content-Type": "application/json",
              "Authorization": f"Bearer {gcp_token}",
            }

    response = requests.post(f'https://iam.googleapis.com/v1/projects/{project_id}/serviceAccounts/{account}:signJwt',json=payload, headers=headers)
    if response.status_code != 200:
        log.error(f"GCP IAM ERROR response: {response.json()}")
        response.raise_for_status()

    return response.json().get('signedJwt')

class OverrideCredentials:
    '''
    This simulates (but does not inherit, nor is it compatible) a google.auth.credentials.Credentials
    object.  Used to hold user overridden values to obtain a signed JWT.

    The values must come from the environment (GCP_TOKEN, PROJECT_ID, ACCOUNT).
    '''

    def __init__(self) -> None:
        self.service_account_email = os.environ.get('ACCOUNT')
        self.project_id = os.environ.get('PROJECT_ID')
        self.token = os.environ.get('GCP_TOKEN')

    @property
    def valid(self):
        ''' True if all attributes are not empty
        '''
        return bool(self.service_account_email and self.project_id and self.token)


if __name__ == "__main__":
    if len(sys.argv) > 1:
        usage()
        sys.exit(1)

    vault_address =  os.environ.get('VAULT_URL') or os.environ.get('VAULT_ADDR')

    if not vault_address:
        log.error("No Vault address specified! Must define either VAULT_URL or VAULT_ADDR in environment")
        sys.exit(1)

    user_creds = OverrideCredentials()

    if user_creds.valid:
        log.warning("Warning: using GCP credentials from environment to get a Vault token")
        creds = user_creds
    else:
        gcp_creds, _ = auth.default(scopes=['https://www.googleapis.com/auth/cloud-platform'])
        if not gcp_creds.valid and gcp_creds.token is None:
            auth_req = auth.transport.requests.Request()
            gcp_creds.refresh(auth_req)
        creds = gcp_creds

    project_id=user_creds.project_id
    if hasattr(creds, 'project_id'):
        project_id=creds.project_id

    if not project_id:
        raise ValueError("google.auth.Credentials object does not have project_id and PROJECT_ID not defined!")

    claim=json.dumps({
                "aud":f"vault/{project_id}",
                "exp":int(time.time()+900),
                "sub":creds.service_account_email,
            })
    if user_creds.valid:
        signed_jwt = get_signed_jwt(claim, project_id, creds.service_account_email, creds.token)
    else:
        iam_client=iam_credentials_v1.IAMCredentialsClient(credentials=creds)
        name = iam_client.service_account_path('-', creds.service_account_email)
        signed_jwt = iam_client.sign_jwt(name=name,payload=claim).signed_jwt

    output =\
    {
        "gcp" : {
            "token" : creds.token,
            "account": creds.service_account_email,
            "project_id": project_id,
            "jwt" : signed_jwt,
            },
        "vault": {
            "token": get_vault_token(signed_jwt, project_id, vault_address)
        },
    }


    print(json.dumps(output))

