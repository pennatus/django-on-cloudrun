import logging

from django.shortcuts import render
from django.http import HttpResponse
from django.conf import settings


logger = logging.getLogger('django')

# Create your views here.

def index(request):
    logger.error('In Hello World index')
    return HttpResponse("Hello, world! You're at the hello main index.")
