from django.test import TestCase
from django.test import Client
from django.http.response import HttpResponse

# Create your tests here.

class HelloIndex(TestCase):
  def setUp(self):
    self.client = Client()
    _setupDB()

  def test_get_hello_index(self):
    response:HttpResponse = self.client.get('/hello/')

    self.assertEqual(response.status_code, 200)


def _setupDB():
  pass