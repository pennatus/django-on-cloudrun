""" Add/Modify the urls below.  These will get added to the urls in urls_common.py """

# Import the default urls.  urls_default.py should not need to be modified
from django.urls import include, path
from django.views.generic import RedirectView

from urls_default import urlpatterns


urlpatterns += [
    path('', RedirectView.as_view(url='/admin')),
    path('hello/', include('apps.hello.urls')),
]
