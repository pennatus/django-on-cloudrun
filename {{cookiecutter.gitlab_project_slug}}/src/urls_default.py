"""django_server URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from django.contrib import admin
from django.conf import settings
from django.conf.urls import url
from django.utils.translation import ugettext_lazy as _
from django.views.generic import RedirectView

urlpatterns = [
    path('admin/', admin.site.urls),
]

if settings.DEBUG:
    import debug_toolbar
    from django.conf.urls.static import static
    urlpatterns += [
        path('__debug__/', include(debug_toolbar.urls)),
    ] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

    def load_module(name:str):
        ''' Find a module by `name` and return it after executing the contents.
        '''
        import importlib.util

        spec = importlib.util.find_spec(name)
        mod = None
        if spec:
            mod = importlib.util.module_from_spec(spec)
            exec_module = None
            if spec.loader:
                exec_module = getattr(spec.loader,'exec_module')
            if exec_module:
                exec_module(mod)

        if not mod:
            raise ImportError(name)

        return mod


    if settings.USE_DRF:
        # Import using importlib just to get around pylint issues when not using DRF
        permissions = load_module('rest_framework.permissions')
        get_schema_view = load_module('drf_yasg.views')
        openapi = load_module('drf_yasg.openapi')

        schema_view = get_schema_view.get_schema_view(
            openapi.Info(title=f'{settings.PRODUCT_NAME} API',
                         default_version='v1'),
            public=True,
            permission_classes=(permissions.AllowAny,),
        )

        urlpatterns += [
            url(r'^swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
        ]

admin.site.site_header = _(settings.PRODUCT_NAME)
admin.site.site_title = _(settings.PRODUCT_NAME)
