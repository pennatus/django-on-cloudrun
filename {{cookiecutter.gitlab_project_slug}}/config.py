import os
import argparse
import logging
import json
from argparse import Namespace
from typing import Dict, List, Optional

import yaml
import hvac
import pyterprise

class EnvVariable:
  ''' EnvVariables are fully managed and should be unset when not used.
  '''
  def __init__(self, name: str, value: Optional[str] = None, description:Optional[str] = None):
    self.name = name
    self.value = value or ''
    self.description = description or ''

  def export(self):
    ''' Print shell formated export statements: `export VAR_NAME=VAR_VALUE`
    '''
    if self.value:
      print(f"export {self.name}={self.value}")

  def unset(self):
    ''' Print shell formated unset statements: `unset VAR_NAME`
    '''
    print(f"unset {self.name}")

  def define(self):
    ''' Print shell formated define statements (Makefile compatible): `VAR_NAME=VAR_VALUE`
    '''
    print(f"{self.name}={self.value}")

class ReadWriteVariable(EnvVariable):
  ''' Read/Write variables are set by the user and should not be unset, but
  do need to be written out to the new environment.
  '''
  def __init__(self, name, **kwargs):
    super().__init__(name, value=os.environ.get(name), **kwargs)

  def unset(self):
    pass

class ReadOnlyVariable(EnvVariable):
  ''' Read only variables are managed exclusively by the user and should
  not be written out.
  '''
  def __init__(self, name: str, **kwargs):
    super().__init__(name, value=os.environ.get(name), **kwargs)

  def export(self):
    pass

  def unset(self):
    pass


class Variables:
  def __init__(self, env_list: Optional[List[EnvVariable]] = None):

    env_list = env_list or []
    self.variables = { variable.name: variable for variable in env_list }

  def set(self, name, value):
    old_value = self.variables[name]
    old_value.value = value
    self.variables[name] = old_value

  def get(self, name):
    return self.variables[name]

  def export(self):
    ''' Export all variables, make sure that unused variables are cleaned by unsetting.
    '''
    for variable in self.variables.values():
      variable.unset()
    for variable in self.variables.values():
      variable.export()

    log.info(f"Exported {len(self)} variables with {self.unused()} not set.")


  def define(self):
    ''' Define all variables in the current environment, will not persist to children unless
    the parent exports the variable name.
    '''
    for variable in self.variables.values():
      variable.define()

    log.info(f"Defined {len(self)} variables with {self.unused()} not set.")

  def unused(self) -> int:
    ''' Returns the number of unused variables
    '''
    return len([variable for variable in self.variables.values() if not variable.value])

  def __len__(self):
    return len(self.variables)

  def __str__(self):
    help_text = [ f'{variable.name} - {variable.description} (default: {variable.value})' for variable in self.variables.values() ]
    return '\n'.join(help_text)

env_list = Variables([
  EnvVariable('APP_DEPLOY_TARGET', description='The desired target for app deployment, ex: local, cloudrun, aws'),
  ReadOnlyVariable('APPROLE_ROLE_ID', description='CI/CD username for Vault'),
  ReadOnlyVariable('APPROLE_SECRET_ID', description='CI/CD password for Vault'),
  EnvVariable('CLOUDSQL_DB_INSTANCE_NAME', description='The coordinates for the CloudSQL database.'),
  EnvVariable('DATABASE_URL', description='A Postgres formatted URL to locate the DB (can contain credentials).'),
  EnvVariable('DB_DEPLOY_TARGET', description='The desired target for DB deployment, ex: local, cloudsql, aws'),
  EnvVariable('DB_INSTANCE_NAME', description='For CloudSQL, the name of the instance hosting the db.'),
  EnvVariable('DB_ORIGIN', description='The coordinates to connect to the postgres database.  Does not include the database name.'),
  EnvVariable('DB_NAME', description='The name of the database.'),
  EnvVariable('DEPLOY_ENV', description='Target environment for operations'),
  EnvVariable('DOCKER_LOCAL_IMAGE', description='The fully qualified image name built locally.'),
  EnvVariable('DOCKER_LOCAL_NETWORK', value='django', description='The name of docker network to connect app to local db.'),
  EnvVariable('DOCKER_PATH', description='The Docker image path for remote prod deployment.'),
  EnvVariable('DOCKER_REGISTRY', description='Target docker registry when deploying.'),
  EnvVariable('DOCKER_REMOTE_IMAGE', description='The fully qualified image name for remote registry.'),
  ReadWriteVariable('GCP_TOKEN', description='CloudSQL/GCP Registry credentials.'),
  ReadOnlyVariable('GITLAB_API_TOKEN', description='Gitlab credentials for access to registry or CI/CD.'),
  EnvVariable('LOG_LEVEL', value='DEBUG', description='Python log level which sets the global default level'),
  EnvVariable('PRODUCT_NAME', description='The end user friendly name of the application'),
  EnvVariable('PROJECT_ID', description='The GCP project ID where the CloudSQL instance is running.'),
  EnvVariable('PROJECT_VERSION', value='latest', description='User defined version of app, tags container and passes into container env.'),
  EnvVariable('PYTHONPATH', value='src', description='Package directory to help tools find  the modules from anywhere.'),
  EnvVariable('SKIP_AUTO_MIGRATE', description='Prevent the auto-migrate script from running on container start'),
  ReadOnlyVariable('TFC_API_TOKEN', description='A Terraform Cloud API Token to authenticate and perform operations.'),
  EnvVariable('GET_VAULT_TOKEN', value='', description='The authentication method of obtaining a Vault token when running make.'),
  EnvVariable('VAULT_DATABASE_PATH', description='Vault path to read dynamic credentials for the DB.'),
  ReadWriteVariable('VAULT_TOKEN', description="Either the user's token, or the exported token for the Django app"),
  EnvVariable('VAULT_URL', description="Required variable for vaulthelpers to locate the Vault instance"),
])

def read_tfc_token():
  with open(os.path.expanduser('~/.terraformrc'),'r') as terraform_rc:
    # File is in HCL format and I haven't found a Python parser
    # Example:
    # credentials "app.terraform.io" {
    #  token = "JWT_string"
    # }
    for line in terraform_rc:
      if 'token' in line:
        token = line.split()[2].replace('"','')
        break
    else:
      raise Exception(f"No token found in {terraform_rc.name}")

    return token

def publish(config: Dict, args: Namespace):
  ''' Publish an image by creating a new CloudRun service
  '''
  version:str=args.version
  tfc_token:str = env_list.get('TFC_API_TOKEN').value or read_tfc_token()

  workspace_name=f'{config["deploy_env"]}-{config["product_name_slug"]}'
  org_name=f'{config["terraform_org_name"]}'
  host="https://app.terraform.io"


  client = pyterprise.Client()
  client.init(token=tfc_token, url=host)

  org = client.set_organization(id=org_name)
  workspace = org.get_workspace(name=workspace_name)

  tf_variables = workspace.list_variables()
  for variable in tf_variables:
    if not args.re_publish and variable.key == 'DEPLOYED_VERSION':
      log.info(f'Setting DEPLOYED_VERSION to {version}')
      variable.update(value=version)

  if args.auto_apply:
      workspace.plan_apply(message=f'Auto-deploy {version}')
  else:
      log.info(f'Skipped plan and apply (use --auto-apply) but version {version} is ready.')

def get_vault_client(args: Namespace, config: Dict) -> hvac.Client:
  ''' Return an authenticated Vault client
  '''
  client = hvac.Client(url=config['vault_addr'])

  if args.get_vault_token == "approle":
    log.info("Fetching Vault token with approle.")
    client.auth_approle(role_id=os.environ['APPROLE_ROLE_ID'],secret_id=os.environ['APPROLE_SECRET_ID'])
  elif args.get_vault_token == "create":
    policies=[f'product-{config["deploy_env"]}-{config["product_name_slug"]}-owner',
              f'db-{config["db_instance_name"]}-{config["deploy_env"]}_{config["product_name_slug"]}-owner',
    ]
    log.info(f"Creating token with policies: {policies}")
    client.create_token(policies=policies,orphan=True, ttl='24h')
  elif args.get_vault_token == "env":
    log.info("Using VAULT_TOKEN from environment")
    client.token = os.environ['VAULT_TOKEN']
  else:
    raise Exception(f"Unknown Vault authentication method: {args.get_vault_token}")

  log.info(f"Found a token from Vault client")

  return client


def print_env(config: Dict[str,str], args):
  '''
  Print a set of environment statements that can be evaulated in a shell

  ex: eval $(config.py print_env)
  '''

  deploy_env = config["deploy_env"]
  log.info(f"Exporting variables for environment:{deploy_env}...")
  log.info(f"Using cloudrun: {args.cloudrun} and cloudsql: {args.cloudsql}")

  env_list.set('PROJECT_VERSION', args.version)
  env_list.set('DEPLOY_ENV', deploy_env)
  env_list.set('PRODUCT_NAME', config["product_name_slug"])
  env_list.set('APP_DEPLOY_TARGET', f"local")
  env_list.set('DB_DEPLOY_TARGET', f"localdb")
  env_list.set('GET_VAULT_TOKEN', args.get_vault_token)
  env_list.set('VAULT_URL', config['vault_addr'])
  docker_registry = 'gcr.io'
  docker_path = f'{config["product_name_slug"]}'
  docker_local_image = f'{docker_path}:{args.version}'
  remote_docker_path = f'{config[f"product_{deploy_env}_project_id"]}/{docker_path}'
  env_list.set('DOCKER_REGISTRY', f'{docker_registry}')
  env_list.set('DOCKER_PATH', docker_path)
  env_list.set('DOCKER_LOCAL_IMAGE', docker_local_image)
  env_list.set('DOCKER_LOCAL_NETWORK', config.get('docker_local_network', 'django'))
  env_list.set('DOCKER_REMOTE_IMAGE', f'{docker_registry}/{remote_docker_path}:{args.version}')

  if args.cloudrun:
    env_list.set('APP_DEPLOY_TARGET', f"cloudrun")

  # Database name defaults to the product name but can be overridden in the config.yaml
  db_name = config.get('db_name', f'{config["product_name_slug"]}')

  if args.cloudsql:
    # DB_NAME must be unique to namespace them on a shared DB server
    db_name = f'{deploy_env}_{db_name}'
    db_origin = f'postgres://127.0.0.1:5432'
    env_list.set('DB_DEPLOY_TARGET', f"cloudsql")
    env_list.set('PROJECT_ID', config["db_instance_project_id"])
    env_list.set('DB_INSTANCE_NAME', config["db_instance_name"])
    env_list.set('CLOUDSQL_DB_INSTANCE_NAME', f'{config["db_instance_project_id"]}:us-central1:{config["db_instance_name"]}')
    env_list.set('VAULT_DATABASE_PATH', f'org/db/{config["db_instance_name"]}/creds/{db_name}-owner')
  else:
    hostname = config.get("db_local_hostname", "postgres")
    username = config.get("db_local_username", "postgres")
    password = config.get("db_local_password", "password")
    db_origin = f'postgres://{username}:{password}@{hostname}'

  env_list.set('DB_ORIGIN', db_origin)
  env_list.set('DB_NAME', db_name)
  env_list.set('DATABASE_URL', f'{db_origin}/{db_name}')

  if args.gcr or args.cloudsql or args.cloudrun:
    # When running locally or deploying from CI/CD, we must provide a GCP_TOKEN and VAULT_TOKEN
    # to authenticate with cloudsql_proxy or gcr.io.
    vault_client = get_vault_client(args, config)
    env_list.set('GCP_TOKEN', vault_client.read(f'/org/gcp/token/{deploy_env}-{config["gcp_roleset"]}')['data']['token'])
    env_list.set('VAULT_TOKEN', vault_client.token)

  if deploy_env != 'dev':
    # We can't risk migrating the db without someone at the helm
    env_list.set('SKIP_AUTO_MIGRATE', 'true')

  env_list.define()

def read_config(args) -> Dict:
  ''' Read in the config.yaml and add `deploy_env` in the dictionary
  '''
  with open('config.yaml','r') as config_file:
    config = yaml.load(config_file, Loader=yaml.FullLoader)
  config['deploy_env'] = args.deploy_env
  return config

def var_list(**kwargs):
  print(env_list)

def get_args() -> Namespace:
  ''' Parse the command line arguments and return a argparse.Namespace object
  '''
  introduction=( f"The 'Environment' manager assists in setting up meaningful combinations of"
                 f"environment variables as well as publish local docker images to CloudRun."
                 f"\n\nYou must have the appropriate credentials to deploy to CloudRun or use"
                 f"CloudSQL.  The supported methods are to autenticate with AppRole (approle),"
                 f" read the value from the environement variable VAULT_TOKEN (env) or to create"
                 f" an orphan token from a currently logged in client."
                 f"\n\nFor example:\n\n    vault login -method oidc\n    config.py print_env -m create"
                 f"\n\n"
  )
  parser = argparse.ArgumentParser(description=introduction, formatter_class=argparse.RawDescriptionHelpFormatter)

  parser.add_argument("command", help="print_env: set environment, publish: create new CloudRun version, var_list: print help for variables", choices=['print_env','publish','var_list'])
  parser.add_argument("-v", "--verbose", help="increase log level to debug", action='store_true')
  parser.add_argument("--cloudrun", help="set deploy target to cloudrun", action='store_true')
  parser.add_argument("--cloudsql", help="set the db to use CloudSQL", action='store_true')
  parser.add_argument("--gcr", help="Get credentials to use GCR", action='store_true')
  parser.add_argument("-e", "--deploy-env", help="set the deploy env target", choices=['dev','stg','prd'],default='dev', type=str)
  parser.add_argument("-m", "--get-vault-token", help="method to get token: token uses VAULT_TOKEN, approle uses APPROLE_ROLE_ID/APPROLE_SECRET_ID and create uses default token.",
                                                 choices=['create','approle','env'], default='create', const='create', type=str, nargs='?')
  parser.add_argument("-a", "--auto-apply", help="automatically plan and apply when running deploy command", action='store_true')
  parser.add_argument("-r", "--re-publish", help="Reuse existing CloudRun version, but force a restart of service", action='store_true')
  parser.add_argument("-V", "--version", help="export version other than latest", default='latest', type=str)

  args = parser.parse_args()

  args.log_level = 'INFO'
  if args.verbose:
    args.log_level = 'DEBUG'

  return args

if __name__ == "__main__":
  args = get_args()
  # Setup logging so we can use the output in a bash eval
  logging.basicConfig(format='# %(message)s', level=args.log_level)
  log = logging.getLogger('manage')
  config = read_config(args)
  command = globals()[args.command]
  command(config=config,args=args)
