#!/bin/sh
# wait-for-postgres.sh

set -e
db_url=${1}/postgres # The DB_URL is composed of the origin and a database to connect to.  The postgres db always exists.
db_name=${2}
retries=0
until psql $db_url -c '\q'; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 1
  retries=$(( retries + 1 ))
  if [ $retries -ge 10 ]; then
    exit 1
  fi
done
psql $db_url -c "create database $db_name;" 2> /dev/null | true
