# Introduction

This is your new Django project backed by CloudRun, Vault and Gitlab.  Customize this README as your project develops.  In the meantime, here are some instructions on how to get started with the project.

## Getting Started

The project is built into a Docker image that can be deployed along side a postgres server either locally or remotely using CloudRun on GCP.

For development, using Vault is not required, but if a new environment (like dev/stg/prd) is added to infrastructre, then the `kv` secret engine will need to be populated with a minimum of three variables. For example:

``` sh
vault kv put product/prd/mywebsite/kv-v2/django/env PRODUCT_NAME=MyOrg SECRET_KEY=super-secret-GUID # you can put other env variables here too!
```

Now you are ready to run the app locally:

``` sh
make
make shell
python src/manage.py migrate
python src/manage.py createadmin    # Defaults to admin/admin for local dev. Don't use in production!
exit
```

Run `make help` for additional targets useful in development.

Visit your blank project and see it working:

1. Visit `http://localhost:8080`
1. Login with admin/admin

## Remote debugging

Not ready yet.

## Runtime Configuration

This project uses environment variables like an "API" for interacting with its django stack.  These environment variables are described below, certain variables are required in different environments and are noted in the table.  Use `python config.py --help` and `python config.py var_list` to learn how to control the combinations.  For example, to connect to remote database with a local `django-server` running make sure you have sufficient Vault credentials (ex: /org/gcp/mywebsite-ow) and run `config.py -e prd --cloudsql`.

Description of when variables need to be configured:

Values for `Required` column:

- GCP: Variable needs to be set when using Cloud Run (only relevant if you are an admin)
- PRD: Variable needs to be set for production environments using CloudSQL/CloudRun
- STG: Variable needs to be set for for staging environments using CloudSQL/CloudRun
- DEV: Variable can be overridden to simulate other users like CI/CD or Terraform deploying to STG or PRD
- CI/CD: Variable must be set by CI/CD to perform deployments
- No/Yes: Variable can be changed to simulate other behaviour (some variables are composited and can't be used independently)

The source code for how the variable is used is listed in the DEV/STG/PRD columns:

- `config.yaml` is the default file written by Cookiecutter and used by `config.py`
- `config.py` is the primary manager of environment variables and is used to produce a `.env` file
- `Terraform` means that an infrastructure file specifies the variable as an input to Cloud Run
- `vault2env` means the variable is set during `start.sh` invocation by `vault2env` helper
- `GCP` means the variable is set during `start.sh` when running on a GCP instance
- `settings.py` means the variable is set by the Django source code
- `.gitlab-ci.yml` means that the Gitlab CI/CD sets the variable
- `Gitlab` means that the Gitlab environment variable must be set
- `01_migrate` is `runparts.d` script that sets the default value

| Variable                   | Required| Managed By  | DEV   | STG       | PRD        | Description  |
|----------------------------|:--------|:------------|:------|:----------|:-----------|:-------------|
| `DEPLOY_ENV`               | GCP     | config.py   | .env  | Terraform | Terraform | The type of environment being deployed to, changes the value of other variables (like Docker container path)
| `GCP_TOKEN`                | No      | config.py    | Vault | GCP       | GCP       | If not specified, it is pulled from Vault or GCP Application Default Credentials
| `VAULT_TOKEN`              | DEV     | config.py    | .env  | Vault     | Vault     | If not specified, it can be obtained from Vault by authenticating with GCP_TOKEN in `start.sh`.  Policy by `start.sh`.  Used to push Docker images to specific environment Docker registry, connecting to a Cloud SQL databse or obtaining vault2env values.
| `DATABASE_URL`             | DEV     | config.py    | .env  | Terraform | Terraform | A `django-environ` formatted URL that includes everything required to connect to the database
| `PROJECT_ID`               | GCP     | config.py    | N/A   | Terraform | Terraform | The GCP project ID that holds the infrastructure. Required to authenticate to Vault using GCP_TOKEN and GCP Auth method (signed JWT).
| `CLOUDSQL_DB_INSTANCE_NAME`| GCP     | config.py    | N/A   | Terraform | Terraform | A GCP formatted set of coordinates to Cloud SQL databases, if set forces DATABASE_URL to localhost.
| `VAULT_DATABASE_PATH`      | GCP     | config.py    | N/A   | Terraform | Terraform | A Vault path that contains DB credentials and read by `vaulthelpers`
| `VAULT_ADDR`               | No      | config.yaml  | .env  | N/A    | N/A       | The address to the Vault instance that config.py uses, does not override Django's address.
| `PRODUCT_NAME`             | STG/PRD | kv on Vault  | .env  | vault2env | vault2env | The name of the product, Ex: "MyWebsite" for prod, "WebSite Dev" for dev.
| `PRODUCT_VERSION`          | No      | config.py    | settings.py | settings.py | settings.py | Set by bump2version and committed to the repository
| `SECRET_KEY`               | PRD     | kv on Vault  | .env  | vault2env | vault2env  | A runtime secret required by Django to salt hashes.
| `APPROLE_SECRET_ID`        | CI/CD   | N/A         | Gitlab | Gitlab    | Gitlab | Used by Gitlab CI/CD to fetch a Vault token, and then a GCP token from Vault to push to the registry.  A sufficiently privileged `VAULT_TOKEN` can also work.
| `APPROLE_ROLE_ID`          | CI/CD   | N/A         | Gitlab | Gitlab    | Gitlab | Used by Gitlab CI/CD  to fetch a Vault token, and then a GCP token from Vault to push to the registry.  A sufficiently privileged `VAULT_TOKEN` can also work
| `PROJECT_VERSION`          | CI/CD   | config.py    | config.py| .gitlab-ci.yml | .gitlab-ci.yml | Sets the Docker version tag
| `DOCKER_REGISTRY`          | No      | config.py    | config.py| config.py | config.py | Sets the Docker registry that hosts the image.
| `GUNICORN_THREADS`         | PRD     | start.sh    | start.sh| Terraform | Terraform | (Overridden by `DEPLOY_ENV`) Number of threads
| `GUNICORN_WORKERS`         | PRD     | start.sh    | start.sh| Terraform | Terraform | (Overridden by `DEPLOY_ENV`) Number of workers (usually matches core count)
| `SKIP_AUTO_MIGRATE`        | PRD     | 01_migrate  | 01_migrate | 01_migrate | Terraform | Forces a migration on container start.  Do not use in production.

## Create Terraform Workspaces

You will need as many Terraform Cloud workspaces as environments.  For each environment create a workspaces, ex: `{dev,stg,prd}-widget`.  Montior the directories within your infrastructure repository based on environment and product.  ex: `prd/widget`, `dev/widget`  The directories will get created later.  If you followed the `README.md` in the original cookiecutter there was a secion on how to use `tf-workspaces.py` located in the root of your infrastructure directory to do this.

For each workspace set `DEPLOYED_VERSION` to `latest` for now (automatically done by `tf-workspaces.py` helper).  This will be used to control the Docker tag that is deployed.  To changed deployed versions, just change the variable and manually queue a new plan.

The next bit of terraform work is to add the infrastructure files for each environment.  In this project directory there is an `infrastructure` directory that contains a working example for production.  That needs to be replicated for each environment you wish to deploy.  Copy the files from the cookiecutter `infrastructure` directory into the terraform managed infrastructure repository with the folder structure specified when creating the Terraform workspaces.  The files assume a production environment and will need to be duplicated for each environemnt and then searched and replaced for the `prd` pattern (don't change the `terraform_remote_state` resources though).

``` sh
# NOTE: assumes a lot about your directory structure, don't copy and paste without grok'ing
product_name=widget # whatever you named this product
cookiecutterOutputPath=~/projects/${product_name}/ # location of this project
for anEnv in "dev" "prd" "stg"; do
  target=${anEnv}/${product_name}
  mkdir $target
  cp -a $cookiecutterOutputPath/infrastructure/. ${target}/.
done
```

Example snippets where `prd` should be replaced in `main.tf`, also check `disabled.remote.tf` (remember! don't change the `terraform_remote_state` resource.):

``` hcl
data "vault_generic_secret" "gcp_token" {
  path = "org/gcp/token/prd-website-ow"
}
```

``` hcl
db_name = "stg_mywebsite"
```

``` hcl
env {
  # One of dev, stg or prd
  name = "DEPLOY_ENV"
  value = "prd"
}
```

Once you're happy, check the README.md file in the directory and then commit your infrastructure which triggers a plan/apply phase.  Once the plan has been reviewed and applied the CloudRun service should be deployed and you can use `terraform output` to get the URL to the service which should have an admin login setup in the previous step.

## Configuring Cloud SQL databases

If this is the first time the database has been created, or if migrations need to be run on production you may need to manually connect to and modify the Cloud SQL databases.  This is the same procedure as bringing up the first instance locally except that `config.py` will be configured to point to the remote database in CloudSQL.

NOTE: This requires a `VAULT_TOKEN` with owner policies to modify the database.

``` sh
make
make shell
python config.py migrate
python config.py createadmin
```

## Workflows

### Create a release

The CI/CD pipelines are set up to push a docker image to the registry when the repository is tagged.  This means a git tag is how a release candidate is pushed.  To deploy will be a Terraform step (see below). Start by creating a release branch, it could be a **minor release branch** from master or a **patch release branch** from a previous release tag.  The release branch is used as an integraton point for QA to find issues and targeted bug fixes to go in.  It should be a short lived branch and get merged back into master as soon as possible.

``` sh
git checkout master
git pull
git checkout -b 1.2.0
git add .
git commit -m "Create release branch"
git push origin 1.2.0  # CI/CD triggers a test
git checkout master
git merge 1.2.0 # CI/CD triggers a build, deploys to CloudRun in the staging environment
git tag 1.2.0
git push --tags # CI/CD triggers a deploy to CloudRun production environment
```

### Deploy a feature branch

If your infrastructure gets complicated, you may want to use a dev workspace to allow posting a feature branch.  The workflow is the same as staging.

1. Tag a commit in your feature branch that represents preview ready work.
1. Set the `DEPLOYED_VERSION` variable to the tag name.
1. Queue a plan in the dev-mywebsite Terraform workspace.
1. Apply plan.
